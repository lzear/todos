/** @type {import('next').NextConfig} */
const nextConfig = {
  experimental: {
    serverActions: true,
  },
}

// eslint-disable-next-line unicorn/prefer-module
module.exports = nextConfig
